// Data archiving service for the Mooring Controller. Stores data records
// and metadata as newline-delimited JSON objects. A new file is created
// on a periodic basis (every hour by default).
package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/mfkenney/go-redisq"
)

var Version = "dev"
var BuildDate = "unknown"

type Datarec struct {
	Name  string      `json:"sensor"`
	Clock [2]uint64   `json:"clock"`
	Data  interface{} `json:"data"`
}

// Write metadata records from a Redis queue to a channel
func mdReader(ctx context.Context, dataq *redisq.Queue) <-chan map[string]interface{} {
	c := make(chan map[string]interface{}, 1)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
			}
			vals, err := dataq.Get(true, 2)
			if err != nil {
				continue
			}

			if md, ok := vals[0].(map[string]interface{}); ok {
				if name, ok := md["name"]; ok {
					_, ok = name.(string)
					c <- md
				} else {
					log.Printf("Invalid type for 'name': %v", name)
				}
			} else {
				log.Printf("Invalid record type: %T", vals[0])
			}
		}
	}()

	return c
}

func isNaN(f float32) bool { return f != f }

// Write data records from a Redis queue to a channel
func dataReader(ctx context.Context, dataq *redisq.Queue) <-chan Datarec {
	c := make(chan Datarec, 1)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
			}
			vals, err := dataq.Get(true, 2)
			if err != nil {
				continue
			}
			sensor, ok := vals[0].(string)
			if !ok {
				log.Printf("Invalid data record: %q", vals)
				continue
			}
			dr := Datarec{Name: sensor}

			switch x := vals[1].(type) {
			case int64:
				dr.Clock[0] = uint64(x)
			case uint64:
				dr.Clock[0] = uint64(x)
			case int32:
				dr.Clock[0] = uint64(x)
			case uint32:
				dr.Clock[0] = uint64(x)
			}

			switch x := vals[2].(type) {
			case int64:
				dr.Clock[1] = uint64(x)
			case uint64:
				dr.Clock[1] = uint64(x)
			case int32:
				dr.Clock[1] = uint64(x)
			case uint32:
				dr.Clock[1] = uint64(x)
			}

			switch data := vals[3].(type) {
			case []float64:
				nan := math.NaN()
				for i := 0; i < len(data); i++ {
					if data[i] == nan {
						data[i] = -1
					}
				}
				dr.Data = data
			case []float32:
				for i := 0; i < len(data); i++ {
					if isNaN(data[i]) {
						data[i] = -1
					}
				}
				dr.Data = data
			default:
				dr.Data = data
			}

			c <- dr
		}
	}()

	return c
}

// Read sensor data and metadata records and write them to a file.
func archiver(ctx context.Context, cdata <-chan Datarec,
	cmeta <-chan map[string]interface{},
	mdcache map[string]map[string]interface{},
	filename, linkdir string) {

	log.Printf("archiver thread starting (%s)", filename)
	// Check if file already exists
	_, err := os.Stat(filename)
	newfile := os.IsNotExist(err)
	// Create all directories if needed, we can safely ignore the
	// returned error because it will be caught when we try to
	// open the file.
	os.MkdirAll(filepath.Dir(filename), 0755)

	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Printf("Cannot open file %s: %v", filename, err)
		return
	}
	enc := json.NewEncoder(f)

	if newfile {
		// This is a new file, write the metadata
		for _, entry := range mdcache {
			err = enc.Encode(entry)
			if err != nil {
				log.Println(err)
			}
		}
	}

	// On closing, link the file into `linkdir` unless it
	// contains no data.
	defer func(d string) {
		info, err := f.Stat()
		f.Close()
		if err == nil && info.Size() == 0 {
			os.Remove(filename)
			return
		}
		if d != "" {
			os.Link(filename,
				filepath.Join(d, filepath.Base(filename)))
		}
	}(linkdir)

	nrecs := int(0)
loop:
	for {
		select {
		case <-ctx.Done():
			break loop
		case md := <-cmeta:
			// Cache new metadata records and append them to the file.
			name := md["name"].(string)
			if _, found := mdcache[name]; !found {
				mdcache[name] = md
				err = enc.Encode(md)
				if err != nil {
					log.Println(err)
				}
			}
		case dr := <-cdata:
			err = enc.Encode(dr)
			if err != nil {
				log.Println(err)
			} else {
				nrecs++
			}
		}
	}
	log.Printf("archiver thread exiting (%s, %d records)", filename, nrecs)
}

// Clock synchronous version of time.Ticker. The first tick will occur
// a the next multiple of the specified interval.
type SyncTicker struct {
	C      chan time.Time
	cancel chan struct{}
}

func NewSyncTicker(interval time.Duration) *SyncTicker {
	tm := SyncTicker{
		C:      make(chan time.Time, 1),
		cancel: make(chan struct{})}

	go func() {
		tnext := time.Now().Truncate(interval).Add(interval).Sub(time.Now())
		select {
		case t := <-time.After(tnext):
			tm.C <- t
		case <-tm.cancel:
			return
		}

		ticker := time.NewTicker(interval)
	loop:
		for {
			select {
			case t := <-ticker.C:
				tm.C <- t
			case <-tm.cancel:
				ticker.Stop()
				break loop
			}
		}
	}()

	return &tm
}

func (t *SyncTicker) Stop() {
	close(t.cancel)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] directory\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Archiver service for sensor data records\n\n")
		flag.PrintDefaults()
	}

	showvers := flag.Bool("version", false,
		"Show program version information and exit")
	dqname := flag.String("dq", "archive:queue",
		"Name of Redis list for the data queue")
	mdqname := flag.String("mdq", "md:queue",
		"Name of Redis list for the metadata queue")
	linkdir := flag.String("link", "",
		"Link each archive file into the specified directory")
	interval := flag.Duration("interval", time.Hour,
		"Archive file creation interval")

	flag.Parse()
	if *showvers {
		fmt.Printf("%s %s\n", os.Args[0], Version)
		fmt.Printf("  Build date: %s\n", BuildDate)
		fmt.Printf("  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}
	topdir := args[0]

	dataq, err := redisq.NewQueue("localhost:6379", *dqname, 0)
	if err != nil {
		panic(err)
	}

	mdq, err := redisq.NewQueue("localhost:6379", *mdqname, 0)
	if err != nil {
		panic(err)
	}

	var (
		ctx, ar_ctx       context.Context
		cancel, ar_cancel context.CancelFunc
		wg                sync.WaitGroup
	)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	ctx, cancel = context.WithCancel(context.Background())
	defer cancel()

	cmeta := mdReader(ctx, mdq)
	cdata := dataReader(ctx, dataq)
	mdcache := make(map[string]map[string]interface{})

	// Archive file naming scheme
	var archive_name func() string
	if *interval >= time.Hour {
		archive_name = func() string {
			t := time.Now().UTC()
			return fmt.Sprintf("%d/%d/data-%s.json",
				t.Year(),
				t.YearDay(),
				t.Format("20060102-15"))
		}
	} else {
		archive_name = func() string {
			t := time.Now().UTC()
			return fmt.Sprintf("%d/%d/data-%s.json",
				t.Year(),
				t.YearDay(),
				t.Format("2006010215-04"))
		}
	}

	tm := NewSyncTicker(*interval)

	ar_ctx, ar_cancel = context.WithCancel(ctx)
	wg.Add(1)
	go func() {
		defer wg.Done()
		archiver(ar_ctx, cdata, cmeta, mdcache,
			filepath.Join(topdir, archive_name()), *linkdir)
	}()

evloop:
	for {
		select {
		case <-tm.C:
			ar_cancel()
			ar_ctx, ar_cancel = context.WithCancel(ctx)
			wg.Add(1)
			go func() {
				defer wg.Done()
				archiver(ar_ctx, cdata, cmeta, mdcache,
					filepath.Join(topdir, archive_name()), *linkdir)
			}()
		case <-sigs:
			break evloop
		}
	}
	ar_cancel()
	log.Println("Waiting for archiver to exit")
	wg.Wait()
}
